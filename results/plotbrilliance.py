import matplotlib.pyplot as plt
import numpy as np

from units import *
from constants import *
from parameters import *
import fields


# Get the data
times = np.load("results/data/times.npy")
energies = np.load("results/data/energies.npy")
amplitudes = np.load("results/data/amplitudes.npy")


def comp_brilliance(energy):
    width = .001 * energy
    brilliance = fields.comp_num_photons(energy-width/2., energy+width/2.,
                                         times, energies, amplitudes)
    brilliance /= (opening_angle/mrad)**2.
    brilliance /= central_cone_area/mm**2.
    return brilliance


# Compute the predicted and simulated brilliances at the harmonic.
try:
    # Get the simulated and predicted harmonics.
    with open("results/data/simulatedharmonic.txt", "r") as temp:
        simulated_harmonic = float(temp.read())
    # Compute the predicted brilliance.
    predicted_brilliance = 1.43e14*num_periods*current*K**2./(1 + K**2./2.)
    predicted_brilliance /= (opening_angle/mrad)**2. * central_cone_area/mm**2.
    print("The predicted brilliance at the first harmonic "
          + f"is {predicted_brilliance :.5} (count/s)/(mm^2⋅mrad^2).")
    # Compute the simulated brilliance.
    simulated_brilliance = comp_brilliance(simulated_harmonic)
    print("The simulated brilliance at the first harmonic "
          + f"is {simulated_brilliance :.5} (count/s)/(mm^2⋅mrad^2).")
    # Compute the error.
    error = abs(1. - simulated_brilliance/predicted_brilliance)
    print("The error of the simulated brilliance from the "
          + f"predicted brilliance is {error*100. :.5}%.")
except FileNotFoundError:
    print("To compute the brilliance at the harmonic, the program "
          "results/plotharmonics.py needs to be ran first.")

# Compute the simulated brilliances at various energies.
central_energies = np.linspace(1.*keV, 80.*keV, 800)
brilliances = np.zeros(central_energies.size)
for i, energy in enumerate(central_energies):
    brilliances[i] = comp_brilliance(energy)
# Plot the brilliances.
plt.plot(central_energies/keV, brilliances)
plt.xlabel("energy (keV)")
plt.ylabel("brilliance ((count/s)/(mm^2⋅mrad^2))")
plt.xlim(1., 80.)
plt.show()
