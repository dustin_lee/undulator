import matplotlib
import matplotlib.pyplot as plt
import numpy as np

from units import *
from constants import *
from parameters import *
import fields


# Get the data.
times = np.load("results/data/times.npy")
electric_field = np.load("results/data/magneticfield.npy")
magnetic_field = np.load("results/data/magneticfield.npy")

# Plot the intensities.
intensities = fields.comp_intensity(electric_field, magnetic_field, times)
# Make a heat map of the intensity.
X, Y = np.meshgrid(x_observation_coords, y_observation_coords)
fig, ax = plt.subplots()
color_bar = ax.pcolormesh(X/mm, Y/mm, intensities/keV,
                          cmap="hot", shading="gouraud")
fig.colorbar(color_bar, ax=ax,
             label="intensity (keV/mm^2)")
# Now, plot the predicted opening of the beam.
domain = np.linspace(0., 2.0*ma.pi, 100)
x = np.array([central_cone_radius*ma.cos(sample) for sample in domain])
y = np.array([central_cone_radius*ma.sin(sample) for sample in domain])
plt.plot(x/mm, y/mm)
# Display the plot.
plt.xlabel("horizontal displacement (mm)")
plt.ylabel("vertical displacement (mm)")
plt.show()
