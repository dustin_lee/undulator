import math as ma
import matplotlib.pyplot as plt
import scipy as sp

from units import *
from constants import *
from parameters import *
import trajectory as traj
import fields


def decompose_fields(times, electric_field):
    # I probably should just use x_comps. Maybe take a perpendicular direction.
    amplitudes = sp.fft.fft(electric_field[:, 0])
    freqs = sp.fft.fftfreq(amplitudes.size, d=(times[1]-times[0]))
    amplitudes = amplitudes[:amplitudes.size//2] #The positive frequencies.
    freqs = freqs[:freqs.size//2]
    energies = freqs*planck_const
    return energies, amplitudes


def comp_harmonic(times, electric_field):
    energies, amplitudes = decompose_fields(times, electric_field)
    idx = amplitudes.argmax()
    max_energy = energies[idx]
    return max_energy


# Get the trajectory.
locations = traj.comp_traj()

# Compute the harmonics by angle.
num_samples = 50
angles = np.linspace(0., 1.5*mrad, num_samples)
predicted_harmonics = np.zeros(num_samples)
simulated_harmonics = np.zeros(num_samples)
for i, angle in enumerate(angles):
    # Predicted harmonic.
    wave_len = spatial_period/(2.*lorentz_factor**2.)
    wave_len *= (1. + K**2./2. + (lorentz_factor*angle)**2.)
    freq = speed_of_light/wave_len
    harmonic = freq*planck_const
    predicted_harmonics[i] = harmonic
    # Simulated harmonic.
    offset = np.array([dist_to_detector*ma.tan(angle), 0., 0.])
    observation_location = detector_location + offset
    times, electric_field, magnetic_field = (
        fields.comp_fields_for_one_electron(locations, observation_location))

    harmonic = comp_harmonic(times, electric_field)
    simulated_harmonics[i] = harmonic

# Print the predicted and simulated first harmonics.
print(f"The predicted first harmonic is {predicted_harmonics[0]/keV :.5} keV.")
print(f"The simulated first harmonic is {simulated_harmonics[0]/keV :.5} keV.")
# Compute the error.
error = abs(1. - simulated_harmonics[0]/predicted_harmonics[0])
print("The error of the simulated harmonic from the predicted harmonic "
      + f"is {error*100. :.5}%.")
# Save the simulated first harmonics to use in plotbrilliance.py.
with open("results/data/simulatedharmonic.txt", "w+") as temp:
    temp.write(str(simulated_harmonics[0]))

# Plot harmonics by angle.
plt.plot(angles/mrad, predicted_harmonics/keV, label="predicted")
plt.plot(angles/mrad, simulated_harmonics/keV, label="simulated", ls="-.")
plt.xlabel("angle (mrad)")
plt.ylabel("energy (keV)")
plt.xlim(0.)
plt.ylim(0.)
plt.legend()
plt.show()


# This is another version, but uses the opening angle as a scale.
# It does not show much information.
#
# # Get the data.
# energies = np.load("results/data/energies.npy")
# amplitudes = np.load("results/data/amplitudes.npy")
#
# # Get the angles.
# x_samples = x_obvservation_coords[num_divs//2:]
# angles = np.tan(x_samples/dist_to_detector)
#
# # Get the predicted harmonics.
# wave_lens = (1. + K**2./2. + (lorentz_factor*angles)**2.)
# wave_lens *= spatial_period/(2.*lorentz_factor**2.)
# freqs = speed_of_light/wave_lens
# predicted_harmonics = freqs*planck_const
#
# # Get the simulated harmonics along the positive x-axis.
# # Also, only take the positive energies and their corresponding amplitudes.
# energies = energies[num_divs//2:, num_divs//2, :energies.size//2]
# amplitudes = amplitudes[num_divs//2:, num_divs//2, :amplitudes.shape[-1]//2]
# indices = amplitudes.argmax(-1)
# simulated_harmonics = energies[np.arange(len(indices)), indices]
#
# # Plot harmonics.
# plt.plot(angles/mrad, predicted_harmonics/keV, label="predicted")
# plt.plot(angles/mrad, simulated_harmonics/keV, label="simulated", ls="-.")
# plt.xlabel("angle (mrad)")
# plt.ylabel("energy (keV)")
# plt.xlim(0.)
# plt.ylim(0.)
# arrowprops = {'facecolor':'black', 'width': .8, 'headwidth':4.,
#               'headlength':5., 'shrink': .05}
# plt.annotate('half of predicted opening angle',
#              xy=((opening_angle/2.)/mrad, 0.),
#              xytext=((opening_angle/2.)/mrad, 2.),
#              arrowprops=arrowprops)
# plt.legend()
# plt.show()
