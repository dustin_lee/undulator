import math as ma
import numpy as np
import random as rand

from units import *
from constants import *


# Independent parameters.
K = .6 #To be an undulator, this needs to be less than 1.
num_periods = 50
dist_to_detector = 30. * meter
start_energy = 2.4*GeV # This does not include the rest mass.
max_mag_field_str = 1.6 * tesla
current = 1. * amp

# Dependent parameters.
spatial_period = K/(0.934*(max_mag_field_str/tesla)) * cm
undulator_len = num_periods * spatial_period
detector_location = np.array([0., 0., dist_to_detector + undulator_len/2.])
start_speed = (speed_of_light
               *(1. - (electron_mass*speed_of_light**2./start_energy)**2.)**.5)
lorentz_factor = 1./ma.sqrt(1. - (start_speed/speed_of_light)**2)
opening_angle = 1./(lorentz_factor*(num_periods**.5)) * rad
central_cone_radius = dist_to_detector * ma.tan(opening_angle/2.)
detector_radius = 3. * central_cone_radius
central_cone_area = ((2.*central_cone_radius))**2.

# Parameters for the numerical methods.
# Computation of the electron trajectory.
num_samples = 10000 # This is only approximate.
time_step = (undulator_len/speed_of_light)/num_samples
# Sampling at the detector.
samples_per_pt = 8192
num_divs = 31 #Number of divisions per side.
sample_area = (2.*detector_radius/num_divs)**2.
assert num_divs%2 == 1
x_observation_coords = np.linspace(-detector_radius, detector_radius, num_divs)
y_observation_coords = np.linspace(-detector_radius, detector_radius, num_divs)
observation_locations = np.zeros((num_divs, num_divs, 2))
for i, x in enumerate(x_observation_coords):
    for j, y in enumerate(y_observation_coords):
        observation_locations[i, j, :] = np.array([x, y])
distances_from_center = (observation_locations**2.).sum(-1)**.5

# Print some parameters.
print(f"The undulator constant is {K :.3}.")
print(f"The spatial period is {spatial_period/cm :.3} cm.")
print(f"The predicted opening angle is {opening_angle/mrad :.3} mrad.")
