import math as ma
import numpy as np

from units import *
from constants import *
from parameters import *


def get_mag_field(location):
    mag_field = np.zeros(3)
    mag_field[1] = (max_mag_field_str
                    *ma.cos(location[2]*(2.*ma.pi/spatial_period)))
    return mag_field


def comp_acceleration(location, velocity):
    mag_field = get_mag_field(location)
    force = electron_charge * np.cross(velocity, mag_field)
    speed = ma.sqrt(np.dot(velocity, velocity))
    lorenz_factor = 1./ma.sqrt(1. - (speed/speed_of_light)**2.)
    acceleration = 1./(electron_mass*lorenz_factor)
    acceleration *= force-(np.dot(force, velocity)*velocity/speed_of_light**2)
    return acceleration


def comp_traj():
    locations = [np.array([0., 0., 0.])]
    velocities = [np.array([0., 0., start_speed])]
    while locations[-1][2] < undulator_len:
        # The first preliminary step, that is, the first half step.
        acceleration1 = comp_acceleration(locations[-1], velocities[-1])
        velocity1 = velocities[-1] + (time_step/2.0)*acceleration1
        location1 = locations[-1] + (time_step/2.0)*velocities[-1]
        # The second preliminary step, that is, the second half step.
        acceleration2 = comp_acceleration(location1, velocity1)
        velocity2 = velocities[-1] + (time_step/2.0)*acceleration2
        location2 = locations[-1] + (time_step/2.0)*velocity1
        # The third preliminary step, that is, the first full step.
        acceleration3 = comp_acceleration(location2, velocity2)
        velocity3 = velocities[-1] + time_step*acceleration2
        location3 = locations[-1] + time_step*velocity1
        # Next state.
        acceleration4 = comp_acceleration(location3, velocity3)
        location = (locations[-1] + (time_step/6.0)*(velocities[-1]
                                                     + 2.0*velocity1
                                                     + 2.0*velocity2
                                                     + velocity3))
        velocity = (velocities[-1] + (time_step/6.0)*(acceleration1
                                                      + 2.0*acceleration2
                                                      + 2.0*acceleration3
                                                      + acceleration4))
        # Check that the electron is not going fater than light.
        assert np.dot(velocity, velocity) < speed_of_light**2.
        # Append the new states.
        locations.append(location)
        velocities.append(velocity)
    locations = np.array(locations)
    return locations
