import math as ma
import numpy as np
import scipy as sp
from scipy.interpolate import interp1d

from units import *
from constants import *
from parameters import *
import trajectory as traj


def comp_fields_for_one_electron(electron_locations, observation_location):
    # This computes the fields using the Heaviside-Feynman Formulas.

    # Get the data to compute the electric field.
    num_samples = len(electron_locations)
    displacements = electron_locations - observation_location
    distances = (displacements*displacements).sum(axis=-1)**.5
    directions = displacements/distances[:, np.newaxis]

    # Compute the electric field.
    # The first term.
    terms1 = directions[1:-1]/(distances[1:-1, np.newaxis]**2.)
    # The second term.
    terms2 = distances[1:-1]/speed_of_light
    ratios = directions/(distances[:, np.newaxis]**2.)
    terms2 = terms2[:, np.newaxis] * (ratios[2:] - ratios[:-2])/(2.*time_step)
    # The third term.
    terms3 = directions[2:] - 2.*directions[1:-1] + directions[:-2]
    terms3 /= time_step**2.
    terms3 /= speed_of_light**2.
    # Combine the terms.
    electric_field = -electron_charge/(4.*ma.pi*eps0) * (terms1+terms2+terms3)

    # Get the magnetic field.
    magnetic_field = -np.cross(directions[1:-1], electric_field/speed_of_light)

    # Get the observations times.
    emit_times = np.linspace(0., time_step*(num_samples-1), num_samples)
    observation_times = emit_times + distances/speed_of_light
    observation_times = observation_times[1:-1] #Make its length the same as
    observation_times -= observation_times[0]       #the length of the fields.

    # Interpolate the fields for evenly spaced observation times.
    E_interp = interp1d(observation_times.copy(), electric_field, axis=0,
                        assume_sorted=True)
    B_interp = interp1d(observation_times.copy(), magnetic_field, axis=0,
                        assume_sorted=True)
    observation_times = np.linspace(observation_times[0],
                                    observation_times[-1],
                                    samples_per_pt)
    electric_field = E_interp(observation_times)
    magnetic_field = B_interp(observation_times)

    return observation_times, electric_field, magnetic_field


def comp_fields(electron_locations, observation_location):
    # This adjusts for interference.

    field_info = comp_fields_for_one_electron(electron_locations,
                                              observation_location)
    observation_times, E, B = field_info

    # Get time shifts of pulses from different electrons.
    rec_time_len = 3.*(observation_times[-1]-observation_times[0])
    num_electrons = current * ((1.*coulomb)/-electron_charge) * rec_time_len
    num_electrons = int(num_electrons + .5)

    # This part adds in the interference.
    # Don't worry about it for now.
    # electric_field = np.zeros((samples_per_pt, 3))
    # magnetic_field = np.zeros((samples_per_pt, 3))
    # for _ in range(num_electrons):
    #     prefix = np.zeros((rand.randint(0, 3*samples_per_pt), 3))
    #     postfix = np.zeros((2*samples_per_pt, 3)) #More than normally needed.
    #     shifted_E = np.concatenate((prefix, E, postfix))
    #     shifted_B = np.concatenate((prefix, B, postfix))
    #     electric_field += shifted_E[samples_per_pt:2*samples_per_pt, :]
    #     magnetic_field += shifted_B[samples_per_pt:2*samples_per_pt, :]

    electric_field = num_electrons * E
    magnetic_field = num_electrons * B

    return observation_times, electric_field, magnetic_field

    ###########################################################################
    # OLD VERSION
    ###########################################################################
    # Get the data to compute the electric field.
    # locations = electron_locations
    # obs_location = observation_location
    # num_samples = len(locations)
    # emit_times = np.linspace(0., time_step*(num_samples-1), num_samples)
    # distances = ((obs_location-locations)**2.).sum(axis=1)**.5
    # obs_times = emit_times + distances/speed_of_light
    # directions = (obs_location-locations)/distances[:, np.newaxis]
    #
    # # Compute the electric field.
    # electric_field = np.zeros((num_samples-2, 3))
    # # The first term.
    # terms1 = directions[1:-1]/(distances[1:-1, np.newaxis]**2.)
    # # The second term.
    # terms2 = distances[1:-1]/speed_of_light
    # ratios = directions/(distances[:, np.newaxis]**2.)
    # terms2 = terms2[:, np.newaxis] * (ratios[2:] - ratios[:-2])/(2.*time_step)
    # # The third term.
    # terms3 = np.ones((num_samples-2, 3))/(speed_of_light**2.)
    # terms3 *= directions[2:] - 2.*directions[1:-1] + directions[:-2]
    # terms3 /= time_step**2.
    # # Combine the terms.
    # electric_field = electron_charge/(4.*ma.pi*eps0)*(terms1 + terms2 + terms3)
    #
    # # Scale by the current.
    # electric_field *= num_electrons_per_coulomb * current
    #
    # # Get the magnetic field.
    # magnetic_field = -np.cross(directions[1:-1], electric_field)/speed_of_light
    #
    # # Adjust the observations times.
    # obs_times = obs_times[1:-1] # to be the same length as the fields.
    # obs_times -= obs_times[0] # to start at 0. (This isn't really needed.)
    #
    # # Readjust the sampling so that each time step is the same and
    # # there are a fixed number of steps.
    # # Save the data computed as originals.
    # org_times = obs_times
    # org_ele_field = electric_field
    # org_mag_field = magnetic_field
    # # Set up the new data.
    # obs_times = np.linspace(0., org_times[-1], samples_per_pt)
    # electric_field = np.zeros((samples_per_pt, 3))
    # magnetic_field = np.zeros((samples_per_pt, 3))
    # # Adjust the new data to the new times.
    # i = 0
    # for j, new_time in enumerate(obs_times):
    #     try:
    #         while org_times[i+1] < new_time:
    #             i += 1
    #         x = (new_time - org_times[i])/(org_times[i+1] - org_times[i])
    #         E = org_ele_field[i] - (org_ele_field[i] - org_ele_field[i+1])*x
    #         B = org_mag_field[i] - (org_mag_field[i] - org_mag_field[i+1])*x
    #         electric_field[j, :] = E
    #         magnetic_field[j, :] = B
    #     except IndexError:
    #         continue # The Value is [0., 0., 0.]
    #
    # return obs_times, electric_field, magnetic_field
    ###########################################################################


def comp_intensity(E, B, times):
    # Integrate to get the total energy.
    intensity = (eps0*E*E/2. + mu0*B*B/2.)
    if E.shape == (num_divs, num_divs, samples_per_pt, 3):
        intensity = intensity.sum(axis=-1).sum(axis=-1)
    elif E.shape == (num_divs, num_divs, samples_per_pt):
        intensity = intensity.sum(axis=-1) #It is just the x-component.
    intensity *= ((times[:, :, 1]-times[:, :, 0])*speed_of_light) * sample_area

    # Scale by current.
    time_widths = times[:, :, -1] - times[:, :, 0]
    intensity /= time_widths

    # intensity = 1./intensity

    return intensity


def get_restricted_fields(energies, amplitudes, lower_bound, upper_bound):
    energies = energies.copy()
    amplitudes = amplitudes.copy()

    # Filter for these energy bounds.
    indices = np.argwhere(((energies>upper_bound) | (energies<lower_bound))
                          &((energies<-upper_bound) | (energies>-lower_bound)))
    amplitudes[indices[:, 0], indices[:, 1], indices[:, 2]] = 0.
    # Filter for the central beam.
    indices = np.argwhere(distances_from_center > central_cone_radius)
    amplitudes[indices[:, 0], indices[:, 1], :] = 0.

    # Take the inverse Fourier transform to get the electric field.
    electric_field = sp.fft.ifft(amplitudes)
    # Get the magnetic field.
    magnetic_field = electric_field/speed_of_light

    return electric_field, magnetic_field


def comp_num_photons(lower_bound, upper_bound,
                     times, energies, amplitudes):
    both_fields = get_restricted_fields(energies, amplitudes,
                                        lower_bound, upper_bound)
    electric_field, magnetic_field = both_fields
    intensity = comp_intensity(electric_field, magnetic_field, times)
    total_energy = intensity.sum()
    mean_energy = (lower_bound+upper_bound)/2.
    num_photons = total_energy/mean_energy
    return float(num_photons)
