from units import *


speed_of_light = 299792458. * meter/sec
electron_mass = 9.1093837015e-31 * kg
electron_charge = -1.60217646e-19 * coulomb
num_electrons_per_coulomb = -1./electron_charge
eps0 = 8.8541878128e-12
mu0 = 1.25663706212e-6
planck_const = 6.62607015e-34 * joule * sec
