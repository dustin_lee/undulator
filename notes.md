# Notes for Simulating an Undulator

## General Notes

## To-Do List

* Figure out why the values are so small.
  - The intensity is way off.
    * **The problem might be with the number of electrons.**
      - What am I actually doing here?
      - The wave propagates over other electrons that are emitting light.
      - How do I actually count the number of electrons?
      - Shouldn't a constant number of electrons still have a beam center? There is a huge peak in the spectrum.
      - **I think the think about all electron positions whose emissions will arrive at the point in a certain time frame.**
        * Wouldn't this increase the intensity in the inverse way of what I want?
  - Try log plots again after I fix this.
  - **Maybe its possible that the trajectory is off.**
* For multiple electrons, I probably need much better statistics.
* Why does the detector diameter affect the speed of the simulation?

* Have ```main.py``` delete previous harmonic data.

* Run a really big simulation.
  - Increase the number of Fourier points used.
  - Increase the number of points used for the trajectory.
    * The shortness of the current trajectory makes this data inaccurate because the edge points are not seen.
  - Increase the number of division.
* Put plots in report.
* Read-me file.
  - Explain how field stuff is done, especially the steady state.
  - Explain that even spacing is needed for the fast Fourier transform.
  - Maybe explain how the data is structured.

* I should probably changes ```samples_per_pt``` to ```num_samples_per_pt```.

* Correctly scale the electric field in ```observe_sample_field.py```.
* Put in units for the permittivity and permeability of vacuum.

* It does not appear that I am getting the correct behavior when I convert this to a wiggler. Look more into this.

* For good accuracy, I should somehow take multiple samples of the fields.
  - The simplest approach would be to just increase the recording time length. Would this be a problem with the Fourier transform?
* I want to get the the pulse length a little more exactly. I could do this by passing more data through ```comp_traj```.
  - Use the variable ```phase_traj```.
    * Return ```times, locations, velocities, accelerations```.
  - I cannot do this because the derivatives are of the direction.
  - Adding more points in the trajectory should help.
    * This does increase the pulse length.
  - If I can figure out the problem with the derivatives of the direction, then I can probably alter the scheme at the boundaries to start and end exactly at the boundaries.
* Can I weigh ```comp_photons``` better accordingly to the energy?
* Should I use a five-step Runge-Kutta scheme?


## Understanding

* How much is taking a Fourier transform over such an irregular field a problem?
  - For the computing the number of photons, this might be a problem.
    * Maybe take a lot of sample points to have more data in the transform.
  - How is this overcome in general? Are restricted frames used?  
* Stepping.
  - What is a helix Runge-Kutta scheme, and how do I use it?
  - Does it make sense to do Runge-Kutta with renormalization?
  - Instead of stepping by time, should I somehow be trying to step by distance?
    * This is probably what Geant4 does.


### Not Directly Related

* How is acceleration of electrons in wires actually accounted for? What is the classical reason that they produce light (even though the field is constant)?
  - They cannot because this would cause superconductors to lose energy?
  - Very classically, electrons don't exist, so acceleration within a wire cannot be seen.
  - So how do loops of wire form antennas? Do antennas only make sense with electrons?
    * It must be from changing the current. Constant current shouldn't produce light.
* Why is the magnetic field proportional to the electric field in Jefimenko's equations?
* How do units affect the Lorenz force? What actually is the Lorenz force law?
  - The unit for the magnetic field has to agree with the unit for the electric field. The Tesla is really N⋅s/(C*m) or kg/(C⋅s) or N/(A⋅m).
  - It's still weird that there isn't a constant, but I guess that the magnetic field is just defined this way.
