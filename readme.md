# Simulation of an Undulator

Author: Dustin Lee Enyeart (dustin.enyeart@protonmail.com)


## About

This project simulates an undulator.
This project is located [here](https://gitlab.com/dustin_lee/undulator).
This one of several simulations of xray sources.
More information is in the file ```report.pdf```.


## How to Run

Version 3.9 of Python is used.

First, the virtual environment needs to be set up.
Instructions for setting up a virtual environment for python are [here](https://packaging.python.org/en/latest/guides/installing-using-pip-and-virtual-environments/#creating-a-virtual-environment).
On GNU/Linux, use
```
python3 -m venv env
source env/bin/activate
pip3 install -r requirements.txt
export PYTHONPATH="${PYTHONPATH}:./source"
```
to set up the virtual environment.
Use ```deactivate``` to leave the virtual environment, and ```source env/bin/activate``` to reenter it.

Then, the path to the directory ```source``` needs to be added. Use
```
export PYTHONPATH="${PYTHONPATH}:./source"
```
to do this. This needs to be done in each shell.

**All programs are meant to be ran from the main directory**. For example, run ```python /results/plotspectrum.py```.

The program ```main.py``` generates the data that is stored in directory ```results/data```. The various programs in the directory ```results``` use this data.


## Physical Set up of Simulation

The electron is traveling along the z-axis. The magnetic field is along y-axis. The electron is accelerated by this magnetic field. This generates an electromagnetic field, which is detected at a detector some distance away from the undulator.


## Code Organization

The physical specifications are contained in the file ```source/parameters.py```.

The directory ```results``` has the code for the plots in ```report.pdf```.

The directory ```tests``` has various tests to check that things are working out correctly. These are mostly visual. They are designed to not depend on the data generated by the program ```main.py```.;


### Trajectory

The function ```comp_traj``` in the directory ```sources/trajectory.py``` computes the trajectory of the electron as it moves through the undulator.
It uses a four-step Runge-Kutta scheme.
The electron is relativistic, and special relativity is required.
The acceleration is computed from this [equation](https://en.wikipedia.org/wiki/Acceleration_(special_relativity)#Acceleration_and_force).
The acceleration on the electron due to radiating light is not accounted for. This effect is negligible.
The only force on the electron is the Lorentz force from the magnetic field.


### Fields

The function ```comp_fields``` in the directory  ```source/fields.py``` computes the fields.
It uses the [Heaviside-Feynman Formulas](https://en.wikipedia.org/wiki/Jefimenko's_equations#Heaviside%E2%80%93Feynman_formula).
The field is computed at evenly spaced time interval so that the Fast Fourier Transform can be applied later.

The function ```get_restricted_fields``` restricts the fields to only the parts that contribute to a specified frequency range.
This is done by apply the Fourier transform to the fields, restricting the frequencies and then applying the inverse Fourier transform.
Because the electric field is polarized, only the x-component needs to be considered.

The function ```comp_intensity```computes the energy in the fields.
This is done by integrating the energy [energy density](https://en.wikipedia.org/wiki/Energy_density#Energy_density_of_electric_and_magnetic_fields) is integrated.
The energy density is integrated with respect to time – rather than space – to account for the part of the wave that reaches the detector within a time period.
The function ```comp_num_photons``` in the directory  ```source/fields.py``` computes the number of photons in the field by computing the intensit and then dividing by the mean energy.
It only works for narrow energy ranges.
