import math as ma
import matplotlib.pyplot as plt
import numpy as np
import scipy as sp

from units import *
from constants import *
from parameters import *
import trajectory as traj
import fields


def decompose_fields(times, electric_field):
    amplitudes = sp.fft.fft(electric_field[:, 0])
    freqs = sp.fft.fftfreq(amplitudes.size, d=(times[1]-times[0]))
    amplitudes = amplitudes[:amplitudes.size//2] #The positive frequencies.
    freqs = freqs[:freqs.size//2]
    energies = freqs*planck_const
    return energies, amplitudes


# Simulate the trajectory of the electron.
electron_locations = traj.comp_traj()

# Plot the spectrum at various angles of a single electron.
for angle in np.linspace(0., 1.*mrad, 5):
    offset = np.array([dist_to_detector*ma.tan(angle), 0., 0.])
    observation_location = detector_location + offset
    field_info = fields.comp_fields_for_one_electron(electron_locations,
                                                     observation_location)
    times, electric_field, _ = field_info
    energies, amplitudes = decompose_fields(times, electric_field)
    plt.plot(energies[:100]/keV, amplitudes[:100],
             label=f"{angle/mrad :.3} mrad")
plt.xlim(0.)
plt.ylim(0.)
plt.legend()
plt.title("Frequencies of a Pulse from One Electron")
plt.show()

# Plot the spectrum at various angles of an electron beam.
for angle in np.linspace(0., 1.*mrad, 5):
    offset = np.array([dist_to_detector*ma.tan(angle), 0., 0.])
    observation_location = detector_location + offset
    field_info = fields.comp_fields(electron_locations, observation_location)
    times, electric_field, _ = field_info
    energies, amplitudes = decompose_fields(times, electric_field)
    plt.plot(energies[:100]/keV, amplitudes[:100],
             label=f"{angle/mrad :.3} mrad")
plt.xlim(0.)
plt.ylim(0.)
plt.legend()
plt.title("Frequencies of a Pulse from Many Electrons")
plt.show()
