import matplotlib.pyplot as plt
import numpy as np

from units import *
from constants import *
from parameters import *
import fields


# Get the data.
times = np.load("results/data/times.npy")
energies = np.load("results/data/energies.npy")
amplitudes = np.load("results/data/amplitudes.npy")

# Compute the spectrum.
energy_spans = [[0.*keV, 10.*keV],
                [10.*keV, 20.*keV],
                [30.*keV, 40.*keV],
                [50.*keV, 60.*keV]
                ]

for span in energy_spans:
    both_fields = fields.get_restricted_fields(energies, amplitudes,
                                               span[0], span[1])
    electric_field, magnetic_field = both_fields

    times_on_axis = times[num_divs//2, num_divs//2, :]
    E = electric_field[num_divs//2, num_divs//2, :]
    B = magnetic_field[num_divs//2, num_divs//2, :]


    plt.plot(times_on_axis, E,
             label=f"{span[0]/keV :.3} – {span[1]/keV :.3} keV")

    #Get the intensity within the restriction.
    intensity = (.5*eps0*E*E + .5*mu0*B*B).sum()
    intensity *= ((times_on_axis[1]-times_on_axis[0])
                  * speed_of_light
                  * sample_area)
    # Scale by current.
    time_width = times_on_axis[-1] - times_on_axis[0]
    intensity /= time_width
    print(f"{span[0]/keV :.3} – {span[1]/keV :.3} keV : "
          + f"{float(intensity)/keV :.3} keV")

plt.legend()
plt.show()
