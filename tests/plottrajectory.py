import math as ma
import matplotlib.pyplot as plt
import numpy as np
import scipy as sp

from units import *
from constants import *
from parameters import *
import trajectory as traj


locations = traj.comp_traj()

# Graph the undulator field and the electron trajectory.
fig, ax1 = plt.subplots()
ax1.set_xlabel("distance along undulator (cm)")
ax2 = ax1.twinx()
# Graph the magnetic field.
domain = np.linspace(0., undulator_len, 4000)
image = []
for z in domain:
    location = np.array([0., 0., z])
    mag_field = traj.get_mag_field(location)
    image.append(mag_field[1])
plot1 = ax1.plot(domain/cm, image, label="magnetic field stength",
                 color="blue")
ax1.set_ylabel("magnetic field (T)", color="blue")
ax1.tick_params(axis="y", labelcolor="blue")
# Graph the displacement of the electron.
domain = locations[:, 2]
image = locations[:, 0]
plot2 = ax2.plot(domain/cm, image/nm, label="electron trajectory", color="red")
ax2.set_ylabel("displacement (nm)", color="red")
ax2.tick_params(axis="y", labelcolor="red")
# Combine the plots.
plots = plot1 + plot2
labels = [plot.get_label() for plot in plots]
plt.legend(plots, labels)
plt.show()

# Compare the start energy and end energy of the electron.
end_velocity = (locations[-1]-locations[-2])/time_step
end_speed = (end_velocity**2.).sum()**.5
end_lorentz_factor = 1./ma.sqrt(1. - (end_speed/speed_of_light)**2.)
end_energy = end_lorentz_factor*electron_mass*speed_of_light**2.
print(f"The start energy is {start_energy/GeV :.5} GeV, "
      f"and the final energy is {end_energy/GeV :.5} GeV.")
