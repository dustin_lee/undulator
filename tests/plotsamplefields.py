import math as ma
import matplotlib.pyplot as plt
import numpy as np

from units import *
from constants import *
from parameters import *
import trajectory as traj
import fields as fields


electron_locations = traj.comp_traj()
observation_location = detector_location

# Plot the field from one electron.
field_info = fields.comp_fields_for_one_electron(electron_locations,
                                                 observation_location)
times, electric_field, magnetic_field = field_info
plt.plot(times/femtosec, electric_field[:, 0], label="x component")
plt.plot(times/femtosec, electric_field[:, 1], label="y component")
plt.plot(times/femtosec, electric_field[:, 2], label="z component")
plt.xlabel("time (femtosec)")
# plt.ylabel("eletric field stength (mV/m)")
plt.title("Electric Field from One Electron at Observation Point")
plt.legend()
plt.show()

# Plot the fields with interference.
field_info = fields.comp_fields(electron_locations, observation_location)
times, electric_field, magnetic_field = field_info
# Plot the electric field.
plt.plot(times/femtosec, electric_field[:, 0], label="x component")
plt.plot(times/femtosec, electric_field[:, 1], label="y component")
plt.plot(times/femtosec, electric_field[:, 2], label="z component")
plt.xlabel("time (femtosec)")
# plt.ylabel("eletric field stength (mV/m)")
plt.title("Electric Field at Observation Point")
plt.legend()
plt.show()
# Plot the magnetic field.
plt.plot(times/femtosec, magnetic_field[:, 0]/tesla, label="x component")
plt.plot(times/femtosec, magnetic_field[:, 1]/tesla, label="y component")
plt.plot(times/femtosec, magnetic_field[:, 2]/tesla, label="z component")
plt.xlabel("time (femtosec)")
plt.ylabel("magnetic field stength (T)")
plt.title("Magnetic Field at Observation Point")
plt.legend()
plt.show()
# See two waves.
observation_location += np.array([.2*cm, 0., 0.])
field_info1 = fields.comp_fields(electron_locations, observation_location)
times1, electric_field1, magnetic_field1 = field_info1
plt.plot(times/femtosec, electric_field[:, 0])
plt.plot(times1/femtosec, electric_field1[:, 0])
plt.xlabel("time (femtosec)")
plt.title("Two Wave Samples")
plt.show()
