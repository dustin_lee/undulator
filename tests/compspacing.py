from units import *
from constants import *
from parameters import *

import trajectory as traj
import fields


angstrom = 1e-10 * meter

num_electron_per_coulomb = (1./-electron_charge)
spacing = speed_of_light*sec/num_electron_per_coulomb
print(f"The mean spacing between the electrons is {spacing/angstrom :.5} Å.")

wave_len = (1. + K**2./2.) * spatial_period/(2.*lorentz_factor**2.)
print(f"Wave lengths are on the order of is {wave_len/angstrom :.5} Å.")

pulse_len = wave_len*num_periods
print(f"Pulse lengths are on the order of is {pulse_len/angstrom :.5} Å.")

times, _, _ = fields.comp_fields_for_one_electron(traj.comp_traj(),
                                                  detector_location)
time_len = times[-1] - times[0]
sim_pulse_len = time_len*speed_of_light
print("The simulated pulse length at the detector "
      f"is {sim_pulse_len/angstrom :.5} Å")

time_len = pulse_len/speed_of_light
print("The time lengths of pulses are "
      + f"on the order of  {time_len/femtosec :.5} fs.")

rec_time_len = 3. * time_len
num_electrons = current * num_electrons_per_coulomb * rec_time_len
num_electrons = int(num_electrons + .5)
print(f"The number of electrons sampled is on the order of {num_electrons}.")
